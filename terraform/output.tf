output "instance_public_ip-master" {
  value = aws_instance.web.*.public_ip
}
